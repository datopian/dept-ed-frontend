const { resolve, URL } = require('url')
const fetch = require('node-fetch')
const appUtils = require('../../../utils')
const utils = require('../utils')

const dms = require('../../../lib/dms')
const config = require('../../../config')
const Model = new dms.DmsModel(config)

class EdDmsModel extends dms.DmsModel {
  constructor(config) {
    super(config)
  }

  async getJSONResponse(url, params) {
    let response = await fetch(url, {
      method: 'POST',
      body: JSON.stringify(params),
      headers: { 'Content-Type': 'application/json' }
    })
    if (response.status !== 200) {
      throw response
    }
    response = await response.json()
    return response
  }

  async getGroup(name) {
    const action = 'group_show'
    let url = new URL(resolve(this.api, action))
    const params = {
      id: name
    }
    let response = await this.getJSONResponse(url, params)
    return appUtils.convertToStandardCollection(response.result)
  }

  async getGroups(options) {
    let action
    if (options.type === 'group') {
      action = 'group_list'
    } else if (options.type === 'organization')  {
      action = 'organization_list'
    } else {
      throw "Called Model.getGroups() with an unsupported group type"
    }
    let url = new URL(resolve(this.api, action))
    const params = {
      all_fields: true
    }
    if (options.query) {
      if (options.query.limit) {
        params.limit = options.query.limit,
        params.offset = options.query.offset
      }
      params.q = options.query.q || ''
      params.sort = options.query.sort || 'title desc'
    }
    let response = await this.getJSONResponse(url, params)
    let countResponse = await this.getJSONResponse(url, {q: params.q})

    const count = countResponse.result.length
    // Convert CKAN group descriptor into "standard" collection descriptor
    const groups = response.result.map(collection => {
      return appUtils.convertToStandardCollection(collection)
    })
    return {
      count,
      results: groups
    }
  }

  async getCollections(query) {
    const action = 'collection_list'
    const queryParams = appUtils.convertToCkanSearchQuery(query)
    let url = new URL(resolve(this.api, action))
    const params = {
      all_fields: true,
      include_extras: true,
      q: queryParams.q,
    }
    let response = await this.getJSONResponse(url, params)
    let countResponse = await this.getJSONResponse(url, {q: params.q})

    const count = countResponse.result.length
    console.log('collections collected', response)
    // Convert CKAN group descriptor into "standard" collection descriptor
    const collections = response.result.map(collection => {
      return appUtils.convertToStandardCollection(collection)
    })
    return {
      count,
      results: collections
    }
  }

  async getCollection(name) {
    const action = 'group_show'
    let url = new URL(resolve(this.api, action))
    const params = {
      id: name,
      all_fields: true,
      include_extras: true,
      include_datasets: true
    }
    let response = await this.getJSONResponse(url, params)

    const collection = utils.convertToEdCollection(response.result)

    return collection
  }


  async getMasterCollections(query) {
    const action = 'master_collection_list'
    const queryParams = appUtils.convertToCkanSearchQuery(query)
    let url = new URL(resolve(this.api, action))
    const params = {
      all_fields: true,
      include_extras: true,
      q: queryParams.q,
    }
    let response = await this.getJSONResponse(url, params)
    // Convert CKAN group descriptor into "standard" collection descriptor
    const collections = response.result.map(collection => {
      return appUtils.convertToStandardCollection(collection)
    })
    return collections
  }

  async getMasterCollection(name) {
    const action = 'group_show'
    let url = new URL(resolve(this.api, action))
    const params = {
      id: name,
      include_extras: true,
      include_datasets: true
    }
    let response = await this.getJSONResponse(url, params)

    const collection = utils.convertToEdCollection(response.result)

    console.log(collection)
    return collection
  }

  async getPackage(name) {
    const action = 'package_show'
    let url = new URL(resolve(this.api, action))
    const params = {
      name_or_id: name,
      include_tracking: true
    }
    let response = await fetch(url, {
      method: 'POST',
      body: JSON.stringify(params),
      headers: { 'Content-Type': 'application/json' }
    })
    if (response.status !== 200) {
      throw response
    }
    response = await response.json()
    return appUtils.ckanToDataPackage(response.result)
  }

  async getOrganizationActivity(name) {
    const action = 'organization_activity_list'
    let url = new URL(resolve(this.api, action))
    const params = {
      id: name,
      limit: 100
    }
    let response = await fetch(url, {
      method: 'POST',
      body: JSON.stringify(params),
      headers: { 'Content-Type': 'application/json' }
    })
    if (response.status !== 200) {
      throw response
    }
    response = await response.json()
    // Convert CKAN group descriptor into "standard" collection descriptor
    return response.result
  }

  async getGroupActivity(name) {
    const action = 'group_activity_list'
    let url = new URL(resolve(this.api, action))
    const params = {
      id: name,
      limit: 100
    }
    let response = await fetch(url, {
      method: 'POST',
      body: JSON.stringify(params),
      headers: { 'Content-Type': 'application/json' }
    })
    if (response.status !== 200) {
      throw response
    }
    response = await response.json()
    // Convert CKAN group descriptor into "standard" collection descriptor
    return response.result
  }

  async getUser(name) {
    const action = 'user_show'
    let url = new URL(resolve(this.api, action))
    const params = {
      id: name,
    }
    let response = await fetch(url, {
      method: 'POST',
      body: JSON.stringify(params),
      headers: { 'Content-Type': 'application/json' }
    })
    if (response.status !== 200) {
      throw response
    }
    response = await response.json()
    // Convert CKAN group descriptor into "standard" collection descriptor
    return response.result
  }
}

module.exports.EdDmsModel = EdDmsModel
