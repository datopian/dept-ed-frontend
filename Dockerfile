FROM node:8

# Create app directory
WORKDIR /usr/src/app

RUN git clone https://github.com/datopian/frontend-v2 .
RUN git clone https://gitlab.com/datopian/dept-ed-frontend.git ./themes/dept-ed-frontend

RUN yarn && cd themes/dept-ed-frontend && yarn && cd -

ENV PLUGINS wp
ENV API_URL https://us-ed-testing.ckan.io/api/3/action/
ENV THEME dept-ed-frontend
ENV NODE_ENV production
ENV WP_URL https://deptofed.wordpress.com
ENV WP_BLOG_PATH /news
ENV BLOCK_ROBOTS true

EXPOSE 4000

CMD [ "yarn", "start" ]

