var gulp = require('gulp');
var sass = require('gulp-sass');
var minCss = require('gulp-minify-css')
var rename = require('gulp-rename')
const postcss = require('gulp-postcss')
// var sourcemaps = require('gulp-sourcemaps'); - Uncomment when developing

sass.compiler = require('node-sass');

// Rebuild css from Sass
gulp.task('sass', function () {
    return gulp.src('public/sass/**/*.scss')
        .pipe(sass({
            outputStyle: 'compressed'
        }).on('error', sass.logError))
        .pipe(gulp.dest('public/css'))

        .pipe(minCss())
        .pipe(rename({
            extname: '.min.css'
        }))
        .pipe(gulp.dest('public/css'))
});

gulp.task('tailwindCompile', function () {
    const postcss = require('gulp-postcss')

    return gulp.src('public/sass/tailwind.css')
        .pipe(postcss([
            require('tailwindcss'),
            require('autoprefixer'),
        ]))
        //.pipe(minCss())
        .pipe(gulp.dest('public/css'))
})

// Watch for LESS file changes
gulp.task('watch', function () {
    gulp.watch(['public/sass/**/*.scss'], gulp.parallel('sass'));
    gulp.watch(['public/sass/*.css'], gulp.parallel('tailwindCompile'));
});


// The default Gulp.js task
gulp.task('default', gulp.parallel('sass', 'watch'));