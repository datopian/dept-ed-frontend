const utils = require("../utils")

const extrasCompact = {
  "collection_type": "master",
  "collections": ["accreditation"]
}
const extras = utils.expandGroupExtras(extrasCompact)

const masterCollection1 = {
  "approval_status": "approved",
  "description": "no description at the moment...",
  "id": "29a82d2f-11c5-48e2-884b-0f34d936bedd",
  "image_display_url": "https://datahub.io/static/img/awesome-data/economic-data.png",
  "image_url": "https://datahub.io/static/img/awesome-data/economic-data.png",
  "is_organization": false,
  "name": "master-collection-1",
  "num_followers": 0,
  "package_count": 2,
  "title": "Master collection 1",
  "display_name": "Master collection 1",
  "type": "group",
  "extras": extras
}

module.exports = { masterCollection1, extras, extrasCompact }