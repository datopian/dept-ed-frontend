'use strict'

const _ = require('lodash')
const { resolve, URL } = require('url')
const fetch = require('node-fetch')
const moment = require('moment')
const nobots = require('express-nobots');

const utils = require('./utils')

module.exports = function (app) {


  const dms = require('./lib/ed-dms')
  const config = app.get('config')
  const appUtils = app.get('utils')

  app.locals = {
    blockRobots: (config.get('BLOCK_ROBOTS') === 'true')
  }

  if (config.get('BLOCK_ROBOTS')) {
    app.use(nobots({block: true}))

    app.get('/robots.txt', function (req, res) {
      res.type('text/plain');
      res.send("User-agent: *\nDisallow: /");
    });
  }

  if (config.get('env') === 'development') {
    const mocks = require('./fixtures')
    mocks.initMocks()
  }

  const Model = new dms.EdDmsModel(config)

  app.get('/', async (req, res) => {
    const groups = await Model.getGroups({type: 'group'})
    const usefulTags = await Model.search({
      'facet.field': '["tags"]',
      'facet.limit': 1000,
      'facet.mincount': 12,
      size: 0
    })
    const recentData = await Model.search({
      q: '',
      size: 5
    })
    const trendingData = await Model.search({
      q: '',
      sort: 'views_total desc',
      size: 5
    })

    const tags = usefulTags.search_facets.tags.items.reverse()

    res.render('home.html', {
      groups: groups.results,
      tags,
      recentData: recentData.results,
      trendingData: trendingData.results
    })
  })

  app.get('/dataset', async (req, res) => {

    let q = req.query.q || ''
    const href = utils.url.getCurrent(req)

    const searchParams = new URL(href).searchParams
    let query = utils.url.toPackageQuery(searchParams)

    const result = await Model.search(query)
    let facets = {}
    let filters = {
      format: [],
      license: []
    }

    const facetsList = ['groups', 'tags', 'organization', 'level_of_data_string']
    utils.search.getFacets({
      facets,
      facetsList,
      result,
      searchParams
    })

    /* console.log(result.search_facets.groups.items) */

    // Populate filters
    _.map(result.results,
      (r) => {
        filters.license.push(r.license)
        _.map(r.resources, (res) => filters.format.push(res.format))
      }
    )
    filters.format = _.pull(_.uniq(filters.format), '')
    filters.license = _.pull(_.uniqWith(filters.license, _.isEqual), undefined)

    // Populate pagination
    const numberOfPages = Math.ceil(result.count / 25)
    const currentPage = parseInt(searchParams.get('page')) || 1
    const pagination = utils.pagination(currentPage, numberOfPages)

    res.render('search.html', {
      q,
      href,
      facets,
      filters,
      currentPage,
      numberOfPages,
      pagination,
      results: result.results,
      count: result.count,
      searchParams: searchParams,
      addParam: utils.url.addParam,
      deleteParam: utils.url.deleteParam,
      setParam: utils.url.setParam,
      qualityMark: utils.qualityMark,
    })
  })

  app.get('/stats', async (req, res) => {
    const url = `${Model.api.replace('api/3/action/', '')}stats/json`

    const stats = await fetch(url)
    const statsJSON = await stats.json()

    res.render('stats.html', {
      stats: statsJSON,
      title: 'Statistics'
    })
  })

  app.get('/dataset/:name', async (req, res) => {

    let name = req.params.name
    const result = await Model.getPackage(name)

    res.render('dataset-data.html', {
      result,
      moment,
      title: 'Data Profile'
    })
  })

  app.get('/dataset/:name/docs', async (req, res) => {
    let name = req.params.name
    const result = await Model.getPackage(name)
    res.render('dataset-docs.html', {
      result,
      moment,
      title: 'Data Documents'
    })
  })

  app.get('/dataset/:name/disqus', async (req, res) => {
    let name = req.params.name
    const result = await Model.getPackage(name)
    res.render('dataset-disqus.html', {
      result,
      moment,
      title: 'Data Discuss'
    })
  })

  app.get('/dataset/:name/usage', async (req, res) => {
    let name = req.params.name
    const result = await Model.getPackage(name)
    res.render('dataset-usage.html', {
      result,
      moment,
      title: 'Data Usage'
    })
  })

  app.get('/collections', async (req, res) => {
    const href = utils.url.getCurrent(req)
    const searchParams = new URL(href).searchParams
    const { q } = req.query
    const pageSize = 21

    let collections
    try {
      collections = await Model.getCollections(req.query)
    } catch (e) {
      // TODO Handle error
      console.error(e)
      collections = {
        count: 0,
        results: []
      }
    }

    const currentPage = parseInt(searchParams.get('page')) || 1
    const numberOfPages = Math.ceil(collections.count / pageSize)
    const pagination = utils.pagination(currentPage, numberOfPages)

    res.render('collections.html', {
      title: 'Collections',
      currentPage,
      numberOfPages,
      pagination,
      searchParams,
      setParam: utils.url.setParam,
      collections: collections.results,
      q
    })
  })

  app.get('/collection/:name', async (req, res) => {
    const { q } = req.query
    const { name } = req.params
    let collection
    try {
      collection = await Model.getCollection(name, { collectionType: "standard" })
    } catch (e) {
      // TODO Handle error
      console.error(e)
    }

    res.render('inner-collection.html', {
      collection,
      q
    })
  })

	app.get('/master-collections', async (req, res) => {
    const { q } = req.query
    let collections
    try {
      collections = await Model.getMasterCollections(req.query)
    } catch (e) {
      // TODO Handle error
      console.error(e)
      collections = []
    }
		res.render('master-collections.html', {
      title: 'Master Collections',
      collections,
      q
		})
	})

	app.get('/master-collection/:name', async (req, res) => {
    const { q } = req.query
    const { name } = req.params
    let collection
    try {
      collection = await Model.getMasterCollection(name)
    } catch (e) {
      // TODO Handle error
      console.error(e)
    }
		res.render('inner-master-collection.html', {
      title: 'Master Collection',
      collection,
      q
		})
	})

  app.get('/publisher', async (req, res) => {

    const pageSize = 21
    const href = utils.url.getCurrent(req)
    const searchParams = new URL(href).searchParams
    let query = {}
    if (searchParams.get('page')) {
      query.offset = ((searchParams.get('page') - 1) * pageSize) || 0
      query.limit = pageSize
    }
    if (searchParams.get('q')) {
      query.q = searchParams.get('q')
    }
    query.sort = searchParams.get('sort')

    const result  = await Model.getGroups({type: 'organization', query})
    const count = result.count
    let organizations = result.results
    if (!query.sort) { // if not manually sorted, sort by count desc
      organizations = _.sortBy(result.results, (r) => -r.count)
    }
    const currentPage = parseInt(searchParams.get('page')) || 1
    const numberOfPages = Math.ceil(count / pageSize)
    const pagination = utils.pagination(currentPage, numberOfPages)

    res.render('publishers.html', {
      href,
      count,
      organizations,
      currentPage,
      numberOfPages,
      pagination,
      searchParams,
      setParam: utils.url.setParam,
      title: 'Publishers'
    })
  })

  app.get('/publisher/:name/activity-stream', async (req, res) => {
    const name = req.params.name
    const query = {
      fq: `organization:"${name}"`,
      'facet.field': ['organization', 'groups', 'tags', 'res_format', 'license_id', 'level_of_data_string'],
    }
    const result  = await Model.search(query)
    const organization = await Model.getProfile(name)
    const activity = await Model.getOrganizationActivity(name)

    /* Key-value store for activity stream items and their slugs */
    const activityMap = {
      'new user': {slug: 'new-user', icon: 'fa-user'},
      'changed user': {slug: 'changed-user', icon: 'fa-user'},
      'new resource': {slug: 'new-resource', icon: 'fa-file'},
      'changed resource': {slug: 'changed-resource', icon: 'fa-file'},
      'new package': {slug: 'new-package', icon: 'fa-sitemap'},
      'changed package': {slug: 'changed-package', icon: 'fa-sitemap'},
      'new organization': {slug: 'new-organization', icon: 'fa-briefcase'},
      'changed organization': {slug: 'changed-organization', icon: 'fa-briefcase'},
      'added tag': {slug: 'added-tag', icon: 'fa-tag'},
    }

    const userIds = _.uniq(activity.map((a) => a.user_id))
    let users = {}
    for (let uid of userIds) {
      users[uid] = await Model.getUser(uid)
    }

    res.render('publisher-activity-stream.html', {
      organization,
      activity,
      result,
      users,
      activityMap,
      moment,
      title: 'Activity Stream'
    })
  })

  app.get('/publisher/:name/about', async (req, res) => {
    const name = req.params.name
    const query = {
      fq: `organization:"${name}"`,
      'facet.field': ['organization', 'groups', 'tags', 'res_format', 'license_id', 'level_of_data_string'],
    }
    const result = await Model.search(query)
    const organization = await Model.getProfile(name)

    res.render('publisher-about.html', {
      result,
      organization,
      title: 'Publisher About'
    })
  })

  app.get('/publisher/:name', async (req, res) => {

    const name = req.params.name
    const q = req.query.q || ''
    const href = utils.url.getCurrent(req)
    const searchParams = new URL(href).searchParams

    let query = utils.url.toPackageQuery(searchParams)
    query.fq = query.fq + ` organization:"${name}"`
    // const query = {
    //   fq: `organization:"${name}"`,
    //   'facet.field': ['organization', 'groups', 'tags', 'res_format', 'license_id', 'level_of_data_string'],
    // }
    const result = await Model.search(query)
    const organization = await Model.getProfile(name)

    let facets = {}

    const facetsList = ['groups', 'tags', 'organization']
    utils.search.getFacets({
      facets,
      facetsList,
      result,
      searchParams
    })

    const numberOfPages = Math.ceil(result.count / 25)
    const currentPage = parseInt(searchParams.get('page')) || 1
    const pagination = utils.pagination(currentPage, numberOfPages)

    res.render('publisher-data.html', {
      result,
      organization,
      href,
      facets,
      currentPage,
      numberOfPages,
      pagination,
      results: result.results,
      count: result.count,
      searchParams: searchParams,
      addParam: utils.url.addParam,
      deleteParam: utils.url.deleteParam,
      setParam: utils.url.setParam,
      qualityMark: utils.qualityMark,
      title: `Publisher (${name})`
    })
  })

  app.get('/topic', async (req, res) => {

    const pageSize = 21
    const href = utils.url.getCurrent(req)
    const searchParams = new URL(href).searchParams
    let query = {}
    if (searchParams.get('page')) {
      query.offset = ((searchParams.get('page')-1) * pageSize) || 0
      query.limit = pageSize
    }
    if (searchParams.get('q')) {
      query.q = searchParams.get('q')
    }
    query.sort = searchParams.get('sort')

    const result  = await Model.getGroups({type: 'group', query})
    const count = result.count
    let groups = result.results
    if (!query.sort) { // if not manually sorted, sort by count desc
      groups = _.sortBy(result.results, (r) => -r.count)
    }
    const currentPage = parseInt(searchParams.get('page')) || 1
    const numberOfPages = Math.ceil(count / pageSize)
    const pagination = utils.pagination(currentPage, numberOfPages)

    res.render('topics.html', {
      href,
      count,
      groups,
      currentPage,
      numberOfPages,
      pagination,
      searchParams,
      setParam: utils.url.setParam,
      title: 'Topics'
    })
  })

  app.get('/topic/:name/activity-stream', async (req, res) => {
    const name = req.params.name
    const query = {
      fq: `organization:"${name}"`,
      'facet.field': ['organization', 'groups', 'tags', 'res_format', 'license_id', 'level_of_data_string'],
    }
    const result  = await Model.search(query)
    const topic = await Model.getGroup(name)
    const activity = await Model.getGroupActivity(name)

    /* Key-value store for activity stream items and their slugs */
    const activityMap = {
      'new user': {slug: 'new-user', icon: 'fa-user'},
      'changed user': {slug: 'changed-user', icon: 'fa-user'},
      'new resource': {slug: 'new-resource', icon: 'fa-file'},
      'changed resource': {slug: 'changed-resource', icon: 'fa-file'},
      'new package': {slug: 'new-package', icon: 'fa-sitemap'},
      'changed package': {slug: 'changed-package', icon: 'fa-sitemap'},
      'new organization': {slug: 'new-organization', icon: 'fa-briefcase'},
      'changed organization': {slug: 'changed-organization', icon: 'fa-briefcase'},
      'added tag': {slug: 'added-tag', icon: 'fa-tag'},
    }

    const userIds = _.uniq(activity.map((a) => a.user_id))
    let users = {}
    for (let uid of userIds) {
      users[uid] = await Model.getUser(uid)
    }

    res.render('topic-activity-stream.html', {
      topic,
      activity,
      result,
      users,
      activityMap,
      moment,
      title: 'Topic Activity Stream'
    })
  })

  app.get('/topic/:name', async (req, res) => {
    const name = req.params.name
    const q = req.query.q || ''
    const href = utils.url.getCurrent(req)
    const searchParams = new URL(href).searchParams

    let query = utils.url.toPackageQuery(searchParams)
    query.fq = query.fq + ` groups:"${name}"`
    const result = await Model.search(query)
    const topic = await Model.getGroup(name)

    let facets = {}
    const facetsList = ['groups', 'tags', 'organization']
    utils.search.getFacets({
      facets,
      facetsList,
      result,
      searchParams
    })

    const numberOfPages = Math.ceil(result.count / 25)
    const currentPage = parseInt(searchParams.get('page')) || 1
    const pagination = utils.pagination(currentPage, numberOfPages)

    res.render('topic-data.html', {
      result,
      topic,
      href,
      facets,
      currentPage,
      numberOfPages,
      pagination,
      results: result.results,
      count: result.count,
      searchParams: searchParams,
      addParam: utils.url.addParam,
      deleteParam: utils.url.deleteParam,
      setParam: utils.url.setParam,
      qualityMark: utils.qualityMark,
      title: 'Topic Datasets'
    })
  })

  app.get('/topic/:name/about', async (req, res) => {
    const name = req.params.name
    const query = {
      fq: `groups:"${name}"`,
      'facet.field': ['organization', 'groups', 'tags', 'res_format', 'license_id', 'level_of_data_string'],
    }
    const result = await Model.search(query)
    const topic = await Model.getGroup(name)
    res.render('topic-about.html', {
      result,
      topic,
      title: 'Topic About'
    })
  })

}
