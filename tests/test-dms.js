const test = require('ava')

const config = require('../../../config')

const mocks = require('../fixtures')
mocks.initMocks()

const dms = require('../lib/ed-dms.js')
const collections = require('../fixtures/collections')
const masterCollections = require('../fixtures/master_collections')

const model = new dms.EdDmsModel(config)

test('getCollections: get standard collections', async t => {
  const collections = await model.getCollections({}, {collectionType: "standard"})
  t.true(Array.isArray(collections))
  t.is(collections[0].name, "accreditation")
  t.is(collections[0].summary, "A collection of economic indicators available on DataHub.")
})

test('getCollections: get standard collections with search query', async t => {
  const collections = await model.getCollections({ q: "accredi" }, {collectionType: "standard"})
  t.true(Array.isArray(collections))
  t.is(collections[0].name, "accreditation")
  t.is(collections[0].summary, "A collection of economic indicators available on DataHub.")
})

test('getCollections: get master collections', async t => {
  const collections = await model.getCollections({}, { collectionType: "master" })
  t.true(Array.isArray(collections))
  t.is(collections[0].name, "master-collection-1")
  t.is(collections[0].summary, "no description at the moment...")
})

test('getCollections: get master collections with search query', async t => {
  const collections = await model.getCollections({ q: "mast" }, { collectionType: "master" })
  t.true(Array.isArray(collections))
  t.is(collections[0].name, "master-collection-1")
  t.is(collections[0].summary, "no description at the moment...")
})

test('getCollection: get a standard collection', async t => {
  const collection = await model.getCollection("accreditation", { collectionType: "standard" })
  t.is(collection.name, "accreditation")
  t.deepEqual(collection.metadata, collections.extrasCompact)
  t.is(collection.collections, undefined)
  t.true(Array.isArray(collection.datasets))
  t.is(collection.summary_dataset.name, "national-student-loan-data-system")
  t.is(collection.datasets[0].name, "national-student-loan-data-system")
})

test.only('getCollection: get a master collection', async t => {
  const collection = await model.getCollection("master-collection-1", { collectionType: "master" })
  t.is(collection.name, "master-collection-1")
  t.deepEqual(collection.metadata, masterCollections.extrasCompact)
  t.is(collection.datasets, undefined)
  t.is(collection.summary_dataset, undefined)
  t.true(Array.isArray(collection.collections))
  t.is(collection.collections[0].name, "accreditation")
  t.is(collection.collections[0].title, "Accreditation")
})
