const test = require('ava')

const utils = require('../utils')

test("compactGroupExtras: one key", t => {
  t.deepEqual(utils.compactGroupExtras([{key: "a", value: "b"}]), {a: "b"})
})

test("compactGroupExtras: empty list", t => {
  t.deepEqual(utils.compactGroupExtras([]), {})
})

test("compactGroupExtras: undefined", t => {
  t.deepEqual(utils.compactGroupExtras(), {})
})

test("expandGroupExtras: one key", t => {
  t.deepEqual(utils.expandGroupExtras({a: "b"}), [{key: "a", value: "b"}])
})

test("expandGroupExtras: empty object", t => {
  t.deepEqual(utils.expandGroupExtras({}), [])
})

test("expandGroupExtras: undefined", t => {
  t.deepEqual(utils.expandGroupExtras(), [])
})

