'use strict'
const _ = require('lodash')
const url = require('./url')
const search = require('./search')

const appUtils = require('../../../utils')


/* Returns an object with quality flags about dataset */
const qualityMark = (dataset) => {
  let flags = {
    machine: false,
    doc: false
  }


  /* Machine readable flag */
  for (let r of dataset.resources) {
    const formats = ['CSV', 'XML']
    const mimetypes = ['text/csv', 'text/json', 'application/json']
    if (formats.includes(r.format)) {
      flags.machine = true
      break
    }
    if (mimetypes.includes(r.mimetype)) {
      flags.machine = true
      break
    }
    if (r.url_type != 'upload' && r.url)
      flags.machine = true
  }

  /* Documentation readable flag */
  for (let r of dataset.resources) {
    if (r.resource_type && r.resource_type == 'doc') {
      flags.doc = true
      break
    }
  }

  return flags
}


const pagination = (page, last, delta=2, dots='...') => {
  let pages = _.concat(
    1,
    _.range(page-delta, page+delta+1),
  )

  pages.push(last)

  pages = _.uniq(
    pages.filter((i) => i > 0 && i <= last)
  )

  let pagination = []

  let prev = 0
  _.each(pages, (i) => {
    if (i - prev > 1) {
      pagination.push(dots)
    }
    pagination.push(i)
    prev = i
  })

  return pagination
}

/**
 * Convert expanded extras to compact form
 *
 * @param {*} extras
 * @returns
 */
function compactGroupExtras(extras) {
  return _.chain(extras).keyBy("key").mapValues(v => v.value).value()
}


/**
 * Convert compact extras to expanded form
 *
 * @param {*} compactExtras
 */
function expandGroupExtras(compactExtras) {
  return Object.entries(compactExtras || {}).map(([key, value])=> ({key, value}))
}


/**
 * Convert a CKAN group to a collection as defined by Dept. of Education.
 *
 * A "standard" collection has a summary dataset and other associated datasets.
 * A "master" collection has other associated collections.
 *
 * @param {*} group group coming from CKAN API
 * @param {*} { collectionType } "standard" or "master"
 * @returns
 */
function convertToEdCollection(group) {
  const collection = appUtils.convertToStandardCollection(group)

  const extrasCompact = compactGroupExtras(group.extras)
  collection.metadata = extrasCompact

  if (group.packages) {
    // ensure resources exists as a list to satisfy ckanToDataPackage
    group.packages = group.packages.map(p => ({ ...p, resources: [] }))
    collection.datasets = group.packages
    .filter(p => {
      const extrasCompact = compactGroupExtras(p.extras)
      return !extrasCompact.summary_dataset
    })
    .map(appUtils.ckanToDataPackage)
  }

  return collection
}

module.exports = {
  qualityMark,
  pagination,
  url,
  search,
  compactGroupExtras,
  expandGroupExtras,
  convertToEdCollection,
}
