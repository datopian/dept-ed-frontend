'use strict'
const _ = require('lodash')
const URL = require('url').URL

const paramsMap = {
  groups: 'topics',
  tags: 'tags',
  organization: 'publishers',
  res_format: 'format',
  license_id: 'license',
  level_of_data_string: 'level_of_data'
}

const toPackageQuery = (queryParams) => {
  const pageSize = 25
  const q = queryParams.get('q')
  let sort = queryParams.get('sort') || 'metadata_modified:desc'
  let from = 0
  if (queryParams.get('page')) {
    from = (queryParams.get('page') - 1) * pageSize
  }

  if (from > 0) {
    // we're not on page 1, so our start should be +1 (e.g. start is 25, we need to start from 26)
    from = from + 1
  }

  let query = {
    q,
    from,
    sort,
    fq: '',
    'facet.limit': 1000,
    'facet.field': ['organization', 'groups', 'tags', 'res_format', 'license_id', 'level_of_data_string'],
    size: pageSize
  }

  for (let facet of _.keys(paramsMap)) {
    let facetParams = queryParams.getAll(paramsMap[facet])
    for (let value of facetParams) {
      query.fq = `${query.fq}${facet}:"${value}" `
    }
  }

  return query
}

const getCurrent = (req) => {
  return `${req.protocol}://${req.get('host')}${req.originalUrl}`
}

const addParam = (href, param, value) => {
  const urlObject = new URL(href)
  urlObject.searchParams.append(param, value)
  return urlObject.href
}

const setParam = (href, param, value) => {
  const urlObject = new URL(href)
  urlObject.searchParams.set(param, value)
  return urlObject.href
}

const deleteParam = (href, param, value) => {
  const urlObject = new URL(href)
  let values = urlObject.searchParams.getAll(param)

  urlObject.searchParams.delete(param)

  for (let oldValue of values) {
    if (oldValue != value)
      urlObject.searchParams.append(param, oldValue)
  }
  return urlObject.href
}

module.exports = {
  paramsMap,
  toPackageQuery,
  getCurrent,
  addParam,
  setParam,
  deleteParam,
}
