'use strict'
const _ = require('lodash')
const URL = require('url').URL
const fetch = require('node-fetch')

const url = require('./url')

const getFacets = (options) => {
  let { facets, facetsList, result, searchParams } = options
  // Populate facet items with check flags and reorder them
  for (let facet of facetsList) {
    _.map(result.search_facets[facet].items,
          (o) => {
            if (searchParams.getAll(url.paramsMap[facet]).includes(o.name))
              o.checked = true
            else
              o.checked = false
          })
    facets[facet] = _.orderBy(
      _.filter(result.search_facets[facet].items,
               (s) => _.keys(result.facets[facet]).includes(s.name)),
      ['checked', 'count', 'name'],
      ['desc', 'desc', 'asc']
    )
  }
}

module.exports = {
  getFacets,
}
